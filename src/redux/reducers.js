// reducers.js
const initialState = {
    form: {
        submittedFormData: null,
        submissionError: null,
    },
};

const formReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SUBMIT_FORM':
            return {
                ...state,
                form: {
                    ...state.form,
                    submittedFormData: action.payload,
                },
            };
        // D'autres cas possibles pour gérer d'autres actions
        default:
            return state;
    }
};

export default formReducer;
