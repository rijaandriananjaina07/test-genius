import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { submitForm } from "./redux/actions";
import FormInput from "./components/FormInput";
import FormTextarea from "./components/FormTextarea";
import { Button, Container, Typography, Grid, Paper, Snackbar } from "@mui/material";
import illustrationImage from "./img.png";
import "./ressource/style.css";

const isValidFrenchPhoneNumber = (phoneNumber) => {
  const phoneNumberRegex = /^(?:(?:(?:\+|00)33[\s.-]?)?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{8})$/;

  if (phoneNumber.trim() !== "" && !phoneNumberRegex.test(phoneNumber)) {
    return false;
  }
  return phoneNumber.length === 12 && phoneNumber.startsWith("+");
};

const App = () => {
  const dispatch = useDispatch();
  const submissionError = useSelector((state) => state.form.submissionError);

  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    message: "",
  });

  const [formErrors, setFormErrors] = useState({
    firstName: "",
    lastName: "",
    email: "",
    phoneNumber: "",
    message: "",
  });

  const [successMessage, setSuccessMessage] = useState("");
  const [isSnackbarOpen, setSnackbarOpen] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));

    setFormErrors((prevFormErrors) => ({
      ...prevFormErrors,
      [name]: "",
    }));
  };

  const validateForm = () => {
    let isValid = true;
    const newFormErrors = {};

    Object.entries(formData).forEach(([key, value]) => {
      if (value.trim() === "") {
        newFormErrors[key] = "Ce champ est requis";
        isValid = false;
      }
    });

    const cleanedPhoneNumber = formData.phoneNumber.replace(/\s/g, "");
    if (
      cleanedPhoneNumber !== "" &&
      !isValidFrenchPhoneNumber(cleanedPhoneNumber)
    ) {
      newFormErrors.phoneNumber =
        "Veuillez entrer un numéro de téléphone français valide.";
      isValid = false;
    }

    setFormErrors(newFormErrors);
    return isValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (!validateForm()) {
      return;
    }

    dispatch(submitForm(formData));

    setSuccessMessage(`Formulaire soumis avec succès! Informations soumises:
      Nom: ${formData.lastName}
      Prénom: ${formData.firstName}
      Email: ${formData.email}
      Numéro de téléphone: ${formData.phoneNumber}
      Message: ${formData.message}
    `);

    setSnackbarOpen(true);

    setFormData({
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      message: "",
    });
  };

  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  return (
    <div className="container">
      <div className="form-container">
        <Container component="main" maxWidth="xs">
          <Paper
            elevation={3}
            style={{
              padding: 16,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Typography variant="h5" gutterBottom>
              React Redux Genius
            </Typography>
            <form onSubmit={handleSubmit} style={{ width: "100%" }}>
              <Grid container spacing={2}>
                <FormInput
                  label="First Name"
                  type="text"
                  name="firstName"
                  value={formData.firstName}
                  onChange={handleChange}
                  error={formErrors.firstName}
                />
                <FormInput
                  label="Last Name"
                  type="text"
                  name="lastName"
                  value={formData.lastName}
                  onChange={handleChange}
                  error={formErrors.lastName}
                />
                <FormInput
                  label="Email"
                  type="email"
                  name="email"
                  value={formData.email}
                  onChange={handleChange}
                  error={formErrors.email}
                />
                <FormInput
                  label="Phone Number"
                  type="text"
                  name="phoneNumber"
                  value={formData.phoneNumber}
                  onChange={handleChange}
                  error={formErrors.phoneNumber}
                />
                <FormTextarea
                  label="Message"
                  name="message"
                  value={formData.message}
                  onChange={handleChange}
                  error={formErrors.message}
                />
              </Grid>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                style={{ marginTop: 16 }}
              >
                Valider
              </Button>
            </form>
            <Snackbar
              open={isSnackbarOpen}
              autoHideDuration={6000}
              onClose={handleSnackbarClose}
              message={successMessage}
            />
          </Paper>
        </Container>
      </div>
      <div className="image-container">
        <img className="image" src={illustrationImage} alt="Illustration" />
      </div>
    </div>
  );
};

export default App;
