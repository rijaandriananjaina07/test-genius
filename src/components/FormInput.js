import React from 'react';
import { Grid, TextField } from '@mui/material';

const FormInput = ({ label, type, name, value, onChange, error }) => {
    return (
        <Grid item xs={12} sm={6}>
            <TextField
                label={label}
                type={type}
                name={name}
                value={value}
                onChange={onChange}
                variant="outlined"
                fullWidth
                error={!!error}
                helperText={error}
            />
        </Grid>
    );
};

export default FormInput;
