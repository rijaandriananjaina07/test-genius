import React from 'react';
import { Grid, TextField } from '@mui/material';

const FormTextarea = ({ label, name, value, onChange, error }) => {
    return (
        <Grid item xs={12}>
            <TextField
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                variant="outlined"
                fullWidth
                multiline
                rows={4}
                error={!!error}
                helperText={error}
            />
        </Grid>
    );
};

export default FormTextarea;
