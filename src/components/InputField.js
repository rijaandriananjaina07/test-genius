// InputField.js
import React from 'react';
import TextField from '@mui/material/TextField';

const InputField = ({ label, type, name, value, onChange, error }) => (
    <div>
        <TextField
            label={label}
            type={type}
            name={name}
            value={value}
            onChange={onChange}
            error={!!error}
            helperText={error}
            fullWidth
            margin="normal"
        />
    </div>
);

export default InputField;
