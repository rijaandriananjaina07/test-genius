# Nom du Projet

Description brève du projet.

## Prérequis

Assurez-vous d'avoir Node.js et npm installés sur votre machine.

## Installation

1. Clonez le projet sur votre machine :
   ```bash
   git clone git@gitlab.com:rijaandriananjaina07/test-genius.git

2. Accédez au répertoire du projet :
    ```bash
    cd nom-du-projet

3. Installez les dépendances :
    ```bash
    npm install

## Utilisation

1. Pour lancer le serveur de développement :

    ```bash
    npm start

Le projet sera accessible à l'adresse http://localhost:3000/ dans votre navigateur par défaut.

